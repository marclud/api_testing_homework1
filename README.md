# School project - API automated tests

## List of exercises
- API automation with Python
- Endpoints with Postman.
- Class initialization.
- Testing with pytest.
