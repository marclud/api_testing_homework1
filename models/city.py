import random
import requests
from faker import Faker

import endpoints

fake = Faker()


class City:

    def __init__(self):
        self.name = fake.uuid4()
        self.population = random.randint(0, 1000000)
        self.id = None
        self.city_url = None

    def get_cities_list(self):
        response = requests.get(endpoints.base_url)
        assert response.status_code == 200
        response_dict = response.json()
        return response_dict

    def create_city(self):
        body = {
            "name": self.name,
            "population": self.population
        }
        create_city_response = requests.post(endpoints.base_url, json=body)
        self.id = create_city_response.json()["id"]
        self.city_url = f"{endpoints.base_url}/{self.id}"  # URL for self.city_url
        assert create_city_response.status_code == 201

    def get_city_information(self):
        city_information = requests.get(self.city_url)
        return city_information

    def response_dict_from_city_information(self):
        response = self.get_city_information()
        assert response.status_code == 200
        response_dict = response.json()
        return response_dict

    def update_city(self):
        self.name = fake.uuid4()
        self.population = random.randint(0, 1000000)
        body = {
            "name": self.name,
            "population": self.population
        }
        update_city_response = requests.put(self.city_url, json=body)
        assert update_city_response.status_code == 200

    def delete_city(self):
        deleted_city_response = requests.delete(self.city_url)
        assert deleted_city_response.status_code == 204
