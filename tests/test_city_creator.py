import pytest

from models.city import City


def test_cities_list(new_city):
    response_dict = new_city.get_cities_list()
    assert new_city.name, new_city.population in response_dict


def test_city_information(new_city):
    response_dict = new_city.response_dict_from_city_information()
    assert new_city.name, new_city.population in response_dict


def test_create_city(new_city):
    response_dict = new_city.get_cities_list()
    assert new_city.name, new_city.population in response_dict


def test_update_city(new_city):
    new_city.update_city()
    response_dict = new_city.response_dict_from_city_information()
    assert new_city.name, new_city.population in response_dict


def test_delete_city():
    city = City()
    city.create_city()
    city.delete_city()
    assert city.get_city_information().status_code == 404


@pytest.fixture
def new_city():
    city = City()
    city.create_city()
    yield city
    city.delete_city()
